﻿Feature: SplitOrderCreation
	
Scenario: SplitOrderCreation

	Given Launch dispatch board and then click neworder button
	And  Then enter parent values on destination, salesalias, purchasealias, supplier, terminal, and Quantity
	| Destination | SalesAlias             | PurchaseAlias | Supplier          | Terminal            | Qty     |
	| Two Eagles  | E10 Reg No Lead/Gallon | E10 RNL       | American Refining | American - Bradford | 1999.99 | 
	Then Click save, the parent order
	Then Go to unscheduled Order List to select the parent order
	When Search newly created parent order to add new chlid for split
	And select the order to add second dentination as child and enter child order values on destination, sa, pa, supp, ter and qty
	| Destination | SalesAlias				| PurchaseAlias | Supplier | Terminal			  | Qty		|
	| Two Eagles  | E10 Reg No Lead/Gallon	| E10 RNL       | Buckeye  | Buckeye-Williamsport | 2599.99 |
	Then split order will created and saved successfully