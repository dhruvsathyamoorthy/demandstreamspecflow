﻿Feature: SingleOrderCreation

Scenario: SingleOrderCreation

	Given Launch dispatch board and then click neworder 
	And  Then enter destination, salesalias, purchasealias, supplier, terminal, and Quantity
	| Destination | SalesAlias             | PurchaseAlias | Supplier          | Terminal            | Qty     |
	| Two Eagles  | E10 Reg No Lead/Gallon | E10 RNL       | American Refining | American - Bradford | 1999.99 |
	Then single order will created and saved successfully


