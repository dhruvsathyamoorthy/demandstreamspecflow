﻿Feature: GroupOrderCreation
	
Scenario: GroupOrderCreation

	Given Launch dispatch board and then click neworderbutton
	And  Then enter parent value on destination, salesalias, purchasealias, supplier, terminal, and Quantity
	| Destination | SalesAlias             | PurchaseAlias | Supplier          | Terminal            | Qty     |
	| Two Eagles  | E10 Reg No Lead/Gallon | E10 RNL       | American Refining | American - Bradford | 1999.99 | 
	Then Click save, the parent order will created successfully
	Then Go to unscheduled order list to select the parent order to add child
	Then Search newly created parent order to add new chlid for group
	And select the order to add second dentination as child order and enter child order values on destination, sa, pa, supp, ter and qty
	| Destination | SalesAlias				| PurchaseAlias | Supplier | Terminal			  | Qty		|
	| Two Eagles  | E10 Reg No Lead/Gallon	| E10 RNL       | Buckeye  | Buckeye-Williamsport | 2599.99 |
	Then Click group order check box to create a group order
	Then group order will created and saved successfully