﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using BoDi;
using OpenQA.Selenium.Remote;
using System;
using System.Reflection;
using TechTalk.SpecFlow;
using PDI.Demandstream.Logistics.Pages.Common;
using PDI.Demandstream.Logistics.Pages;
using System.Threading;
using OpenQA.Selenium;

namespace PDI.Demandstream.Logistics.Tests.Hooks
{
    [Binding]
    public class Hooks : LoginPage
    {
        //Global Variable for Extend report
        private static ExtentTest featureName;
        private static ExtentTest scenario;
        private static ExtentReports extent;
        private static ExtentKlovReporter klov;

        public IObjectContainer _objectContainer = LoginPage._objectContainer;

        public RemoteWebDriver _driver = LoginPage._driver;

        [BeforeTestRun]
        public static void InitializeReport()
        {
            //Initialize Extent report before test starts
            var htmlReporter = new ExtentHtmlReporter(@"D:\Demandstream\SpecFlowProjects\repos\PDI.Demandstream.Automation.Test\TestResults\ExtentReport.html");
            htmlReporter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            //Attach report to reporter
            extent = new ExtentReports();
            klov = new ExtentKlovReporter();

            klov.InitMongoDbConnection("localhost", 27017);
            klov.ProjectName = "PDI.Demandstream.Logistics";

            // URL of the KLOV server
            klov.InitKlovServerConnection("http://localhost:5689");
            klov.ReportName = "Demandstream Automation Test Report" + DateTime.Now.ToString();

            extent.AttachReporter(htmlReporter);
            extent.AttachReporter(klov);
        }



        [BeforeFeature]
        public static void BeforeFeature()
        {
            LoginPage loginPage = new LoginPage();
            loginPage.LaunchDemandStreamApp();
            //Create dynamic feature name
            featureName = extent.CreateTest<Feature>(FeatureContext.Current.FeatureInfo.Title);

        }


        [BeforeScenario]
        public static void Initialize()
        {

            //Create dynamic scenario name
            scenario = featureName.CreateNode<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
        }

        [AfterStep]
        public static void InsertReportingSteps()
        {

            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();

            PropertyInfo pInfo = typeof(ScenarioContext).GetProperty("ScenarioExecutionStatus", BindingFlags.Instance | BindingFlags.Public);
            MethodInfo getter = pInfo.GetGetMethod(nonPublic: true);
            object TestResult = getter.Invoke(ScenarioContext.Current, null);

            if (ScenarioContext.Current.TestError == null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text);
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text);
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text);

            }
            else if (ScenarioContext.Current.TestError != null)
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
            }

            //Pending Status
            if (TestResult.ToString() == "StepDefinitionPending")
            {
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
                else if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
                else if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");

            }

        }

        [AfterScenario]
        public static void After()
        {
            LoginPage._driver.Navigate().Refresh();
            Thread.Sleep(1600);

        }


        [AfterFeature]
        public static void CleanUp()
        {
            LoginPage._driver.Quit();
        }

        [AfterTestRun]
        public static void TearDownReport()
        {
            //Flush report once test completes
            extent.Flush();
        }
    }

}
