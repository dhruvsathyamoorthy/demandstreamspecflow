﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using PDI.Demandstream.Logistics.Pages.ManageShift;
using System;
using TechTalk.SpecFlow;

namespace PDI.Demandstream.Logistics.Tests.Steps.ManageShift
{
    [Binding]
    public class CreateSingleShiftSteps 
    {
        ManageShiftPage obj = new ManageShiftPage();
        OrderCreationPage obj1 = new OrderCreationPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        [Given(@"Lauch demanstream app")]
        public void GivenLauchDemanstreamApp()
        {
            
        }
        
        [Then(@"Go to dispatch board")]
        public void ThenGoToDispatchBoard()
        {
            obj1.DispatchBoard();
        }
        
        [Then(@"Create single shift")]
        public void ThenCreateSingleShift()
        {
            obj.CreateSingleShift();
        }
    }
}
