﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Models;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Demandstream.Logistics.Tests.Steps.SchedulingOrder
{
    [Binding]
    public class ScheduleDispatchtoDriverSteps
    {
        OrderCreationPage obj = new OrderCreationPage();
        DispatchBoardPage obj1 = new DispatchBoardPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        [Given(@"Lauch Dispatch board")]
        public void GivenLauchDispatchBoard()
        {
            obj.DispatchBoard();
        }

        [Then(@"Check whether dispatch to driver board is selected")]
        public void ThenCheckWhetherDispatchToDriverBoardIsSelected()
        {
            
            obj1.DispatchToDriverBoard();
        }
        
        [Then(@"select order from unscheduled order and edit trip time")]
        public void ThenSelectOrderFromUnscheduledOrderAndEditTripTime(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();
            obj1.RanadomOrderandEditTripTime(objDest.TripTime);
        }
        
        [Then(@"Schedule order to dispatch to driver board")]
        public void ThenScheduleOrderToDispatchToDriverBoard()
        {
            obj1.ScheduleOrderToDispatchToDriverBoard();
        }

        //Step Argument
        [StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }
    }
}
