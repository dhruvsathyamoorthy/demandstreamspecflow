﻿using System;
using TechTalk.SpecFlow;

namespace PDI.Demandstream.Logistics.Tests.Steps.SchedulingOrder
{
    [Binding]
    public class ScheduletoCCBSteps
    {
        [Given(@"Lauch DispatchBoard module")]
        public void GivenLauchDispatchBoard()
        {
            
        }
        
        [Then(@"Check whether common carrier is selected")]
        public void ThenCheckWhetherCommonCarrierIsSelected()
        {
            
        }
        
        [Then(@"select order from unscheduled order list and edit trip time")]
        public void ThenSelectOrderFromUnscheduledOrderListAndEditTripTime(Table table)
        {
            
        }
        
        [Then(@"Schedule order to common carreir board")]
        public void ThenScheduleOrderToCommonCarreirBoard()
        {
            
        }
    }
}
