﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Models;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Demandstream.Logistics.Tests.Steps.SchedulingOrder
{
    [Binding]
    public class ScheduletoTruckSteps : BasePage
    {
        OrderCreationPage obj = new OrderCreationPage();
        DispatchBoardPage obj1 = new DispatchBoardPage();

        public RemoteWebDriver _driver = LoginPage._driver;
        [Given(@"Launch dispatch board")]
        public void GivenLaunchDispatchBoard()
        {
            obj.DispatchBoard();
        }
        
        [Then(@"select order from unscheduledorderlist and edit trip time")]
        public void ThenSelectOrderFromUnscheduledorderlistAndEditTripTime(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();
            //TestScript obj = new TestScript();
            obj1.RanadomOrderandEditTripTime(objDest.TripTime);
        }
        
        [Then(@"Schedule order to truck board")]
        public void ThenScheduleOrderToTruckBoard()
        {
            obj1.ScheduleOrderToTruck();
        }

        //Step Argument
        [StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }
    }
}
