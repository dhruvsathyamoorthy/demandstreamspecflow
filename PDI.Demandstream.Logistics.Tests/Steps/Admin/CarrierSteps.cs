﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Admin;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using TechTalk.SpecFlow;

namespace PDI.Demandstream.Logistics.Tests.Steps.Admin
{
    [Binding]
    public class CarrierSteps
    {
        CarrierPage obj = new CarrierPage();
        AdminPage obj1 = new AdminPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        [Given(@"Go to Admin Page")]
        public void GivenGoToAdminPage()
        {
            obj1.AdminMode();
        }
        
        [Given(@"Select Carrier Page")]
        public void GivenSelectCarrierPage()
        {
            obj.RegionalCarrierModule();
            
        }
        
        [Then(@"Update the external carrier information")]
        public void ThenUpdateTheExtreanalCarrierInformation()
        {
            obj.EditRegionalCarrier(); 
            obj.RegionalCarrierUpdate();
        }

        //Step Argument
        /*[StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }*/

    }
}
