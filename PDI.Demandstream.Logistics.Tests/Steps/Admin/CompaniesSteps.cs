﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Admin;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using TechTalk.SpecFlow;

namespace PDI.Demandstream.Logistics.Tests.Steps.Admin
{
    [Binding]
    public class CompaniesSteps
    {
        CompaniesPage obj = new CompaniesPage();
        AdminPage obj1 = new AdminPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        [Given(@"Go to Admin")]
        public void GivenGoToAdmin()
        {
            obj1.AdminMode();
        }
        
        [Given(@"Select Companies Page")]
        public void GivenSelectCompaniesPage()
        {
            obj.Companies();
        }
        
        [Then(@"new company added successfully")]
        public void ThenNewCompanyAddedSuccessfully()
        {
            obj.AddCompanies();
        }
    }
}
