﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Models;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Demandstream.Logistics.Tests.Steps.OrderCreation
{
    [Binding]
    public class GroupOrderCreationSteps
    {
        OrderCreationPage obj = new OrderCreationPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        [Given(@"Launch dispatch board and then click neworderbutton")]
        public void GivenLaunchDispatchBoardAndThenClickNeworderbutton()
        {
            obj.DispatchBoard();
            obj.DispatchBoardNewOrder();
        }
        [Given(@"Then enter parent value on destination, salesalias, purchasealias, supplier, terminal, and Quantity")]
        public void GivenThenEnterParentValueOnDestinationSalesaliasPurchasealiasSupplierTerminalAndQuantity(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();

            //TestScript obj = new TestScript();

            obj.AddDestination(objDest.Destination);
            obj.AddSalesAlias(objDest.SalesAlias);
            obj.AddPurchaseAlias(objDest.PurchaseAlias);
            obj.AddSupplier(objDest.Supplier);
            obj.AddTerminal(objDest.Terminal);
            obj.Quantity(objDest.Qty);
        }
        [Then(@"Click save, the parent order will created successfully")]
        public void ThenClickSaveTheParentOrderWillCreatedSuccessfully()
        {
            obj.ApplyPopupOkay();
            // obj.CloseOrderCreationWindow();
        }
        [Then(@"Go to unscheduled order list to select the parent order")]
        [Then(@"Go to unscheduled order list to select the parent order to add child")]
        public void ThenGoToUnscheduledOrderListToSelectTheParentOrderToAddChild()
        {
            //obj.UnscheduledOrderList();
        }

        [Then(@"Search newly created parent order to add new chlid for group")]
        public void ThenSearchNewlyCreatedParentOrderToAddNewChlidForGroup()
        {
            //obj.UnscheduledOrderListSort();
            //obj.UnscheduledOrderEditOrderContextMenu();
        }

        [Then(@"select the order to add second dentination as child order and enter child order values on destination, sa, pa, supp, ter and qty")]
        public void ThenSelectTheOrderToAddSecondDentinationAsChildOrderAndEnterChildOrderValuesOnDestinationSaPaSuppTerAndQty(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();

            //TestScript obj = new TestScript();

            obj.AddDestination1(objDest.Destination);
            obj.AddSalesAlias4(objDest.SalesAlias);
            obj.AddPurchaseAlias4(objDest.PurchaseAlias);
            obj.AddSupplier4(objDest.Supplier);
            obj.AddTerminal4(objDest.Terminal);
            obj.Quantity4(objDest.Qty);

        }
        [Then(@"Click group order check box to create a group order")]
        public void ThenClickGroupOrderCheckBoxToCreateAGroupOrder()
        {
            obj.GroupOrderCheckBox();
        }
        [Then(@"group order will created and saved successfully")]
        public void ThenGroupOrderWillCreatedAndSavedSuccessfully()
        {
            obj.ApplyPopupOkay();
            obj.CloseOrderCreationWindow();
        }

        //Step Argument
        [StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }
    }
}
