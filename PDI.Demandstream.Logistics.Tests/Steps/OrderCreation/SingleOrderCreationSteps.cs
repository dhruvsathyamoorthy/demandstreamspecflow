﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Models;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Demandstream.Logistics.Tests.Steps.OrderCreation
{
    [Binding]
    public class SingleOrderCreationSteps
    {
        OrderCreationPage obj = new OrderCreationPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        //Scenario -001 Single Order Creation
        [Given(@"Launch dispatch board and then click neworder")]
        public void GivenLaunchDispatchBoardAndThenClickNeworder()
        {
            obj.DispatchBoard();
            obj.DispatchBoardNewOrder();
        }

        [Given(@"Then enter destination, salesalias, purchasealias, supplier, terminal, and Quantity")]
        public void GivenThenEnterDestinationSalesaliasPurchasealiasSupplierTerminalAndQuantity(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();

            obj.AddDestination(objDest.Destination);
            obj.AddSalesAlias(objDest.SalesAlias);
            obj.AddPurchaseAlias(objDest.PurchaseAlias);
            obj.AddSupplier(objDest.Supplier);
            obj.AddTerminal(objDest.Terminal);
            obj.Quantity(objDest.Qty);
        }

        [Then(@"single order will created and saved successfully")]
        public void ThenSingleOrderWillCreatedAndSavedSuccessfully()
        {
            obj.ApplyPopupOkay();
            obj.CloseOrderCreationWindow();

        }

        //Step Argument
        [StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }

    }
}
