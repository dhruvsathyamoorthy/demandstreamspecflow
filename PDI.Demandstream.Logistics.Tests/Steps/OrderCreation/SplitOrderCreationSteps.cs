﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Models;
using PDI.Demandstream.Logistics.Pages;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Demandstream.Logistics.Tests.Steps.OrderCreation
{
    [Binding]
    public class SplitOrderCreationSteps
    {
        OrderCreationPage obj = new OrderCreationPage();

        public RemoteWebDriver _driver = LoginPage._driver;

        //Scenario -002 Split Order Creation
        [Given(@"Launch dispatch board and then click neworder button")]
        public void GivenLaunchDispatchBoardAndThenClickNeworderButton()
        {
            obj.DispatchBoard();
            obj.DispatchBoardNewOrder();
        }

        [Given(@"Then enter parent values on destination, salesalias, purchasealias, supplier, terminal, and Quantity")]
        public void GivenThenEnterParentValuesOnDestinationSalesaliasPurchasealiasSupplierTerminalAndQuantity(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();

            obj.AddDestination(objDest.Destination);
            obj.AddSalesAlias(objDest.SalesAlias);
            obj.AddPurchaseAlias(objDest.PurchaseAlias);
            obj.AddSupplier(objDest.Supplier);
            obj.AddTerminal(objDest.Terminal);
            obj.Quantity(objDest.Qty);
        }

        [Then(@"Click save, the parent order")]
        public void ThenClickSaveTheParentOrder()
        {
            obj.ApplyPopupOkay();
            //obj.CloseOrderCreationWindow();
        }

        [Then(@"Go to unscheduled Order List to select the parent order")]
        public void ThenGoToUnscheduledOrderListToSelectTheParentOrder()
        {
            //obj.UnscheduledOrderList();
        }

        [When(@"Search newly created parent order to add new chlid for split")]
        public void WhenSearchNewlyCreatedParentOrderToAddNewChlidForSplit()
        {
            //obj.UnscheduledOrderListSort();
            //obj.UnscheduledOrderEditOrderContextMenu();
        }

        [When(@"select the order to add second dentination as child and enter child order values on destination, sa, pa, supp, ter and qty")]
        public void WhenSelectTheOrderToAddSecondDentinationAsChildAndEnterChildOrderValuesOnDestinationSaPaSuppTerAndQty(List<OrderCreationModel> ListObj)
        {
            OrderCreationModel objDest = ListObj.FirstOrDefault();

            //TestScript obj = new TestScript();

            obj.AddDestination1(objDest.Destination);
            obj.AddSalesAlias4(objDest.SalesAlias);
            obj.AddPurchaseAlias4(objDest.PurchaseAlias);
            obj.AddSupplier4(objDest.Supplier);
            obj.AddTerminal4(objDest.Terminal);
            obj.Quantity4(objDest.Qty);
        }

        [Then(@"split order will created and saved successfully")]
        public void ThenSplitOrderWillCreatedAndSavedSuccessfully()
        {
            obj.ApplyPopupOkay();
            obj.CloseOrderCreationWindow();
        }


        //Step Argument
        [StepArgumentTransformation]
        public List<OrderCreationModel> Table(Table table)
        {
            return table.CreateSet<OrderCreationModel>().ToList();
        }

        [StepArgumentTransformation]
        public OrderCreationModel Table1(Table table)
        {
            return table.CreateInstance<OrderCreationModel>();
        }
    }
}
