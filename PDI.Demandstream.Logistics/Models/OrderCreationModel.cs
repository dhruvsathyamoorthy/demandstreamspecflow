﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Demandstream.Logistics.Models
{
    public class OrderCreationModel
    {

        public string Destination { get; set; }
        public string SalesAlias { get; set; }
        public string PurchaseAlias { get; set; }
        public string Supplier { get; set; }
        public string Terminal { get; set; }
        public string Qty { get; set; }
        public string OrderNo { get; set; }
        public string TripTime { get; set; }
    }
}
