﻿using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class SOXRefPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public SOXRefPage(RemoteWebDriver driver) => _driver = driver;
    }
}
