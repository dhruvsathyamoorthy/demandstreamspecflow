﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Linq;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    public class CompaniesPage : BasePage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public CompaniesPage(RemoteWebDriver driver) => _driver = driver;
        
        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Companies Admin Module
        By CompaniesModule => By.XPath("//a[contains(text(),'Companies')]");
        By AddCompany => By.XPath("//*[@id='mainNav']/div[4]/div[1]/ul[1]/li[1]/span[1]/a[1]");
        By CompanyName => By.XPath("//input[@placeholder='Enter Name']");
        By CompanyAdd1 => By.XPath("//input[@placeholder='Enter Address1']");
        By City => By.XPath("//input[@placeholder='Enter City']");
        By State => By.XPath("//*[@id='addModalView']/div[6]/div[1]/button[1]/span[1]");
        By StateDropList => By.XPath("/html/body/div[17]/div/div/input");
        By ZipCode => By.XPath("//input[@placeholder='Enter Zipcode']");
        By Phone => By.XPath("//input[@placeholder='Enter Phone']");
        By CustomerType => By.XPath("//span[@class='filter-option pull-left'][contains(text(),'Select Customer Type')]");
        By CustomerTypeList => By.XPath("//div[@class='btn-group bootstrap-select open']//li[3]//a[1]");
        By ExtCompanyID => By.XPath("//input[@placeholder='Enter Ext Company ID']");
        By ExtFccID => By.XPath("//input[@placeholder='Enter Ext FCC ID']");
        By RefreshInterval => By.XPath("//input[@placeholder='Enter Refresh Interval']");
        By AddSaveBt => By.XPath("//button[@class='btn btn-primary']");
        By CheckBox => By.XPath("//input[@class='ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched']");
        By InlineEditIcon => By.XPath("//tr[@class='adminTables siteRow ng-scope delRow']//i[@class='fa fa-pencil']");
        By InlineAlias => By.XPath("//input[@class='form-control ng-pristine ng-valid ng-not-empty ng-touched']");
        By InlineAddress => By.XPath("//tr[contains(@class,'adminTables siteRow ng-scope delRow')]//td[5]//input[1]");
        By InineCity => By.XPath("//input[contains(@class,'form-control reqField ng-pristine ng-valid ng-not-empty ng-touched')]");
        By InlineState => By.XPath("//tr[contains(@class,'adminTables siteRow ng-scope delRow')]//button[contains(@class,'btn dropdown-toggle btn-default')]");
        By InlineStateDropList => By.XPath("//li[contains(@class,'selected active')]//a[contains(@class,'ng-binding ng-scope')]");
        By InilineZipCode => By.XPath("//tr[contains(@class,'adminTables siteRow ng-scope delRow')]//td[8]//input[1]");
        By InlinePhone => By.XPath("//tr[contains(@class,'adminTables siteRow ng-scope delRow')]//td[9]//input[1]");
        By InlineSaveBt => By.XPath("//a[@class='btn btn-sm btn-primary m-t-1']");
        By OkayBt => By.XPath("//button[contains(text(),'Okay')]");

        public CompaniesPage()
        {

        }

        public void Companies()
        {
           
            WaitUntilElementIsVisible(CompaniesModule, _driver);
           
            IWebElement elements = _driver.FindElement(CompaniesModule);
            Actions cursor = new Actions(_driver);
            cursor.MoveToElement(elements).Click().Perform();


        }

        public void AddCompanies()
        {
           

            WaitUntilElementIsVisible(AddCompany, _driver);
            _driver.FindElement(AddCompany).Click();
            WaitForPage();

            WaitUntilElementIsVisible(CompanyName, _driver);
            _driver.FindElement(CompanyName).SendKeys("AutomationTestCompany");

            WaitUntilElementIsVisible(CompanyAdd1, _driver);
            _driver.FindElement(CompanyAdd1).SendKeys("AutomationTestAddress");

            WaitUntilElementIsVisible(City, _driver);
            _driver.FindElement(City).SendKeys("AutomationTestCity");

            WaitUntilElementIsVisible(State, _driver);
            _driver.FindElement(State).Click();

            WaitUntilElementIsVisible(StateDropList, _driver);
            _driver.FindElement(StateDropList).SendKeys("Ohio");
            _driver.FindElement(StateDropList).SendKeys(Keys.Enter);

            WaitUntilElementIsVisible(ZipCode, _driver);
            _driver.FindElement(ZipCode).SendKeys("0123");

            WaitUntilElementIsVisible(CompanyAdd1, _driver);
            _driver.FindElement(CompanyAdd1).SendKeys("9999999999");

            WaitUntilElementIsVisible(AddSaveBt, _driver);
            _driver.FindElement(AddSaveBt).Click();

            WaitUntilElementIsVisible(OkayBt, _driver);
            _driver.FindElement(OkayBt).Click();

        }

    }
}
