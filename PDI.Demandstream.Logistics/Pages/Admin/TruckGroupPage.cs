﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{ 
    public class TruckGroupPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public TruckGroupPage(RemoteWebDriver driver) => _driver = driver;
        //Truck Group
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
