﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class CustomersPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public CustomersPage(RemoteWebDriver driver) => _driver = driver;
        //Customers Admin Module
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
