﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class TerminalsPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public TerminalsPage(RemoteWebDriver driver) => _driver = driver;
        //Terminals
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
