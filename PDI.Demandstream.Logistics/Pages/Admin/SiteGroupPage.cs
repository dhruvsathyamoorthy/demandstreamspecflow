﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class SiteGroupPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public SiteGroupPage(RemoteWebDriver driver) => _driver = driver;
        //Site Group 
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
