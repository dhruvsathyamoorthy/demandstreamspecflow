﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class TanksPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public TanksPage(RemoteWebDriver driver) => _driver = driver;
        //Tanks
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
