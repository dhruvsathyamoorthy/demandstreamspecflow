﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{ 
    class DriversPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public DriversPage(RemoteWebDriver driver) => _driver = driver;
        //Drivers
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
