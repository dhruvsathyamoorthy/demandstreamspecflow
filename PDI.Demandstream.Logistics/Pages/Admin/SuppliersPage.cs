﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class SuppliersPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public SuppliersPage(RemoteWebDriver driver) => _driver = driver;
        //Suppliers
        By Carrier => By.XPath("//a[@title='Carrier']");

    }
}
