﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class UsersPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public UsersPage(RemoteWebDriver driver) => _driver = driver;
        //Users
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
