﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Linq;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    public class CarrierPage : BasePage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public CarrierPage(RemoteWebDriver driver) => _driver = driver;


        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Carrier Admin Module

        By UserMenuList => By.XPath("//a[@class='dropdown-toggle pad-lt-rt-0 header-un']//i[@class='fa fa-sort-desc']");
        By Admin => By.XPath("//a[contains(text(),'Admin')]");
        //By Carrier => By.LinkText("Carrier");
        By Carrier => By.XPath("//*[@id='mainNav']/div[2]/div[1]/div[1]/ul[1]/ul[1]/li[1]/a[1]");
        //By CarrierType => By.XPath("//a[@id='filter_product_txt']");
        By CarrierType => By.XPath("//*[@id='mainNav']/div[4]/div[1]/ul[1]/li[1]/ul[1]/li[1]");
        By CarrierTypeList => By.XPath("//*[@id='mainNav']/div[4]/div[1]/ul[1]/li[1]/ul[1]/li[1]/ul[1]/li[1]/a[1]");
        By CompanyColumnSort => By.XPath("//tr[@class='adminTablesHead']/th[2]/div[1]/a[1]/i[1]");
        By CarrierInlineEditBT => By.XPath("//tr[1]//td[9]//div[1]//span[2]//a[1]");
        By CarrierBoardToggleBT => By.XPath("//tr[1]//td[6]//label[1]//span[2]");
        By CarrierBarToggleBT => By.XPath("//tr[1]//td[7]//label[1]//span[1]");
        By SaveBT1 => By.XPath("//a[@class='btn btn-sm btn-primary m-t-1']");
        By TimeZoneDropDwn1 => By.XPath("//tr[1]//td[8]//div[2]//div[1]//button[1]//span[2]");
        By TimeZoneDropDwnList => By.XPath("//div[@class='btn-group bootstrap-select ng-pristine ng-untouched ng-valid ng-empty ng-not-empty open']//li[3]//a[1]");
        By InLineCloseBT => By.XPath("//a[@class='btn btn-sm btn-default m-t-1']//i[@class='fa fa-close']");
        By SaveBt => By.XPath("//button[@class='btn btn-primary btn-shadow']");

        //Filter
        By ComapnyFilter => By.XPath("//th[2]//div[1]//div[1]//a[1]//i[1]");


        //Regional Carrier
        By RegionalCarrier => By.LinkText("Regional Offices");
        By RegionalCarrierInlineEdit => By.XPath("//tr[1]//td[7]//div[1]//span[2]//a[1]");

        By RegionalCheckButton => By.XPath("//tr[1]//td[1]//div[1]//input[1]");
        By RegionalCarrierEdit => By.XPath("//a[contains(text(),'Edit Regional Office')]");
        By RegionalOfficeCode => By.XPath("//div[@id='addModalView']//div[2]//input[1]");
        By RegionalOfficeDescription => By.XPath("//div[@class='modal m-t-16p fade ng-scope ng-isolate-scope in']//div[3]//input[1]");
        By RegionalCarrierSave => By.XPath("//button[@class='btn btn-primary']");
        By RegionalCarrierOkay => By.XPath("//button[@class='btn btn-primary btn-shadow']");
        

        //Carrier Option
        By CarrierOption => By.LinkText("Carrier Options");

        public CarrierPage()
        {

        }

        
        public void RegionalCarrierModule()
        {
            try
            {
                WaitUntilElementIsVisible(RegionalCarrier, _driver);
                IWebElement elements = _driver.FindElement(RegionalCarrier);
                Actions cursor = new Actions(_driver);
                cursor.MoveToElement(elements).Click().Perform();

                logger.Info("Entering into Regional Carrier Module");
                isAlertpresent(_driver);
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);
            }
        }
        public void EditRegionalCarrier()
        {
            try
            {
                WaitUntilElementIsVisible(RegionalCheckButton, _driver);
                IWebElement elements = _driver.FindElement(RegionalCheckButton);
                Actions cursor = new Actions(_driver);
                cursor.MoveToElement(elements).Click().Perform();

                WaitUntilElementToBeClickable(RegionalCarrierEdit, _driver);
                IWebElement elements1 = _driver.FindElement(RegionalCarrierEdit);
                Actions cursor1 = new Actions(_driver);
                cursor1.MoveToElement(elements1).Click().Perform();

            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        public void RegionalCarrierUpdate()
        {
            WaitUntilElementIsVisible(RegionalOfficeCode, _driver);
            _driver.FindElement(RegionalOfficeCode).SendKeys("AutomationRegionalCarrierCode");

            WaitUntilElementIsVisible(RegionalOfficeDescription, _driver);
            _driver.FindElement(RegionalOfficeDescription).SendKeys("AutomationRegionalCarrierCode");

            WaitUntilElementIsVisible(RegionalCarrierSave, _driver);
            _driver.FindElement(RegionalCarrierSave).Click();

            WaitUntilElementIsVisible(RegionalCarrierOkay, _driver);
            _driver.FindElement(RegionalCarrierOkay).Click();

        }
    }
}