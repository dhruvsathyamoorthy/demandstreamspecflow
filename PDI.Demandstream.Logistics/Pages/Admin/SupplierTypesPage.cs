﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class SupplierTypesPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public SupplierTypesPage(RemoteWebDriver driver) => _driver = driver;
        //Supplier Types
        By Carrier => By.XPath("//a[@title='Carrier']");
       
    }
}
