﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class TrucksPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public TrucksPage(RemoteWebDriver driver) => _driver = driver;
        //Trucks
        By Carrier => By.XPath("//a[@title='Carrier']");

    }
}
