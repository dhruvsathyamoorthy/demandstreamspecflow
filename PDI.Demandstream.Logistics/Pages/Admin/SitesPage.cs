﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class SitesPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public SitesPage(RemoteWebDriver driver) => _driver = driver;
        //Sites
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
