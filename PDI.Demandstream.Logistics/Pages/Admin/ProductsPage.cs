﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class ProductsPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public ProductsPage(RemoteWebDriver driver) => _driver = driver;
        //Products Admin Module
        By Carrier => By.XPath("//a[@title='Carrier']");
    }
}
