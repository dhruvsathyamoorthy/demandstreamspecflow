﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;

namespace PDI.Demandstream.Logistics.Pages.Admin
{
    class ProfilesPage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public ProfilesPage(RemoteWebDriver driver) => _driver = driver;
        //Profiles
        By Carrier => By.XPath("//a[@title='Carrier']");

    }
}
