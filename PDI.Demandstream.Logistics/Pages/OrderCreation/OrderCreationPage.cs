﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Demandstream.Logistics.Pages
{

    public class OrderCreationPage : BasePage
    {
        public static string Order_To_Search;

        public RemoteWebDriver _driver = LoginPage._driver;
        public OrderCreationPage(RemoteWebDriver driver) => _driver = driver;

        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Dispatch Board Module
        By DispatchBoardButton => By.LinkText("Dispatch Board");

        //Create New Order Button
        By NeworderDispatchBoard => By.Id("createNewOrder");

        //Add Destination Button(+)
        By AddDestinationBT => By.XPath("//a[@id='add-desti-anchor']");

        //Site Name Search
        By EnterSiteNameSearch => By.XPath("//*[@id='oc-add-desination-1']/div/span[2]");
        By EnterSiteNameSearch2 => By.XPath("//div[@id='oc-add-desination-2']//i[@class='fa fa-search']");

        //Site Name Text Area
        By SearchSiteName => By.Id("search_site_name_spf");

        //Search Site Button
        By SearchSiteButton => By.Id("search_sites");

        //Site search Result
        By SearchSiteResult => By.XPath("//table[@id='site-search-result']/tbody/tr[1]/td[1]");

        //Sales Alias Search Icon
        By SearchSalesAliasIcon1 => By.CssSelector("#product_tr_1>td>div>span>a.search-sales-alias");
        By SearchSalesAliasIcon2 => By.CssSelector("#product_tr_2>td>div>span>a.search-sales-alias");
        By SearchSalesAliasIcon3 => By.CssSelector("#product_tr_3>td>div>span>a.search-sales-alias");
        By SearchSalesAliasIcon4 => By.CssSelector("#product_tr_4>td>div>span>a.search-sales-alias");
        By SearchSalesAliasIcon5 => By.CssSelector("#product_tr_5>td>div>span>a.search-sales-alias");
        IWebElement searchSalesAliasIcon6 => _driver.FindElementByCssSelector("#product_tr_6>td>div>span>a.search-sales-alias");
        IWebElement searchSalesAliasIcon7 => _driver.FindElementByCssSelector("#product_tr_7>td>div>span>a.search-sales-alias");


        IWebElement searchSalesAliasSearchTextBox => _driver.FindElementByCssSelector("#product_search_code");
        IWebElement searchSalesAliasSearchButton => _driver.FindElementByCssSelector("#search_sales_alias");
        IWebElement sASearchResult => _driver.FindElementByXPath("//table[@id='prod-search-result']/tbody/tr[1]/td[1]");

        //Purchase Alias
        IWebElement purchaseAlias1 => _driver.FindElementByCssSelector("#oc_purch_hold_1>div>button");
        IWebElement purchaseAlias2 => _driver.FindElementByCssSelector("#oc_purch_hold_2>div>button");
        IWebElement purchaseAlias3 => _driver.FindElementByCssSelector("#oc_purch_hold_3>div>button");
        IWebElement purchaseAlias4 => _driver.FindElementByCssSelector("#oc_purch_hold_4>div>button");
        IWebElement purchaseAlias5 => _driver.FindElementByCssSelector("#oc_purch_hold_5>div>button");
        IWebElement purchaseAlias6 => _driver.FindElementByCssSelector("#oc_purch_hold_6>div>button");
        IWebElement purchaseAlias47 => _driver.FindElementByCssSelector("#oc_purch_hold_7>div>button");

        //Supplier
        IWebElement supplier1 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_1']/div/button/span[1]");
        IWebElement supplier2 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_2']/div/button/span[1]");
        IWebElement supplier3 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_3']/div/button/span[1]");
        IWebElement supplier4 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_4']/div/button/span[1]");
        IWebElement supplier5 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_5']/div/button/span[1]");
        IWebElement supplier6 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_6']/div/button/span[1]");
        IWebElement supplier7 => _driver.FindElementByXPath("//*[@id='oc_sup_hold_7']/div/button/span[1]");

        //Terminal
        IWebElement terminal1 => _driver.FindElementByXPath("//*[@data-id='supply_point_1']");
        IWebElement terminal2 => _driver.FindElementByXPath("//*[@data-id='supply_point_2']");
        IWebElement terminal3 => _driver.FindElementByXPath("//*[@data-id='supply_point_3']");
        IWebElement terminal4 => _driver.FindElementByXPath("//*[@data-id='supply_point_4']");
        IWebElement terminal5 => _driver.FindElementByXPath("//*[@data-id='supply_point_5']");
        IWebElement terminal6 => _driver.FindElementByXPath("//*[@data-id='supply_point_6']");
        IWebElement terminal7 => _driver.FindElementByXPath("//*[@data-id='supply_point_7']");

        //Quantity
        IWebElement quantity1 => _driver.FindElementByXPath("//*[contains(@id,'quantity_1')]");
        IWebElement quantity2 => _driver.FindElementByXPath("//*[contains(@id,'quantity_2')]");
        IWebElement quantity3 => _driver.FindElementByXPath("//*[contains(@id,'quantity_3')]");
        IWebElement quantity4 => _driver.FindElementByXPath("//*[contains(@id,'quantity_4')]");
        IWebElement quantity5 => _driver.FindElementByXPath("//*[contains(@id,'quantity_5')]");
        IWebElement quantity6 => _driver.FindElementByXPath("//*[contains(@id,'quantity_6')]");
        IWebElement quantity7 => _driver.FindElementByXPath("//*[contains(@id,'quantity_7')]");

        //Order Apply Button 
        By NewOrderApplyButton => By.CssSelector("#btn-apply");

        //Alert Window 
        By ApplyPOPupOKButton => By.CssSelector(".buttons>button");

        //Order Save Button
        By OrderSaveButton => By.CssSelector("#btn-save");

        //Order Note
        IWebElement orderItemNote1 => _driver.FindElementById("order_note_1");
        IWebElement orderItemNote2 => _driver.FindElementById("order_note_2");
        IWebElement orderItemNote3 => _driver.FindElementById("order_note_3");
        IWebElement orderItemNote4 => _driver.FindElementById("order_note_4");
        IWebElement orderItemNote5 => _driver.FindElementById("order_note_5");
        IWebElement orderItemNote6 => _driver.FindElementById("order_note_6");
        IWebElement orderItemNote7 => _driver.FindElementById("order_note_7");

        //Unscheduled Order List
        IWebElement ordersnoSortUnscheduledOrderList => _driver.FindElementById("s_ono");
        IWebElement ordersnoFilterUnscheduledOrderList => _driver.FindElementById("f_ono");
        IWebElement ordernoFilterTextboxUnscheduledOrderList => _driver.FindElementById("ng-filter-orderno");
        IWebElement unscheduledListContextMenuEditOrder => _driver.FindElementByXPath("//*[@id='db-uso-context-menu']/ul/li[1]/a");
        IWebElement dispatchBoardDispatchboardButton => _driver.FindElementById("s_ono");
        IWebElement unscheduledListContextMenuEditTripTime => _driver.FindElementByXPath("//div[@id='db-uso-context-menu']/ul/li[3]/a");
        By closeOrderCreationWindow => By.XPath("//button[@class='close']");
        IWebElement groupOrderChechBox => _driver.FindElementByXPath("//input[@id='isGroupOrder']");

        public OrderCreationPage()
        {

        }

        public void DispatchBoard()
        {
            try
            {
                if (!_driver.Url.Contains("dispatchboard"))
                {
                    WaitUntilElementIsVisible(DispatchBoardButton, _driver);
                    _driver.FindElement(DispatchBoardButton).Click();
                    logger.Info("TC_OC - Entering into Dispatch Board Module");
                    isAlertpresent(_driver);
                }

            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }

        public void DispatchBoardNewOrder()
        {

            try
            {
                isAlertpresent(_driver);
                WaitUntilElementIsVisible(NeworderDispatchBoard, _driver);
                _driver.FindElement(NeworderDispatchBoard).Click();
                logger.Info("TC_OC - Dispatch Board Module: New Order module is clicked to create fresh order");
                WaitForPage();
            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }

        public void AddDestination(string Destination)
        {
            try
            {
                if (EnterSiteNameSearch != null)
                {

                    WaitUntilElementIsVisible(EnterSiteNameSearch, _driver);
                    _driver.FindElement(EnterSiteNameSearch).Click();
                    logger.Info("TC_OC - Search destination button is clicked to search site name");

                }
                else
                {
                    Console.WriteLine("element not present -- so it entered the else loop");
                    logger.Info("No such element - so it entered into the else loop");

                }
                WaitForPage();
                WaitUntilElementIsVisible(SearchSiteName, _driver);
                _driver.FindElement(SearchSiteName).SendKeys(Destination);

                WaitUntilElementIsVisible(SearchSiteName, _driver);
                _driver.FindElement(SearchSiteName).SendKeys(Keys.Enter);
                logger.Info("TC_OC - Site Name is entered for site search" + "(" + Destination + ")");
                WaitForPage();

                WaitUntilElementIsVisible(SearchSiteButton, _driver);
                _driver.FindElement(SearchSiteButton).Click();
                logger.Info("TC_OC - Search site button is clicked");
                WaitForPage();

                WaitUntilElementIsVisible(SearchSiteResult, _driver);
                _driver.FindElement(SearchSiteResult).Click();
                logger.Info("TC_OC - Searched site is selected from search result as destination");
                WaitForPage();


            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }

        public void AddDestination1(string Destination)
        {
            try
            {
                if (AddDestinationBT != null)
                {
                    WaitForPage();
                    WaitUntilElementIsVisible(AddDestinationBT, _driver);
                    _driver.FindElement(AddDestinationBT).Click();


                    WaitForPage();
                    WaitUntilElementIsVisible(EnterSiteNameSearch2, _driver);
                    _driver.FindElement(EnterSiteNameSearch2).Click();
                    logger.Info("TC_OC - Search destination button is clicked to search site name");
                    WaitForPage();
                }
                else
                {
                    Console.WriteLine("element not present -- so it entered the else loop");
                    logger.Info("No such element - so it entered into the else loop");
                    //WaitForPage();
                }
                WaitUntilElementIsVisible(SearchSiteName, _driver);
                _driver.FindElement(SearchSiteName).SendKeys(Destination);
                _driver.FindElement(SearchSiteName).SendKeys(Keys.Enter);
                logger.Info("TC_OC - Site Name is entered for site search" + "(" + Destination + ")");
                WaitForPage();

                WaitUntilElementIsVisible(SearchSiteButton, _driver);
                _driver.FindElement(SearchSiteButton).Click();
                logger.Info("TC_OC - Search site button is clicked");
                WaitForPage();

                WaitUntilElementIsVisible(SearchSiteResult, _driver);
                _driver.FindElement(SearchSiteResult).Click();
                logger.Info("TC_OC - Searched site is selected from search result as destination");
                WaitForPage();


            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }
        public void AddSalesAlias(string SalesAlias)
        {
            try
            {
                IList<IWebElement> SearchSalesAlias = _driver.FindElements(By.CssSelector("#product_tr_1>td>div>span>a.search-sales-alias"));
                if (SearchSalesAlias.Count != 0)
                {
                    WaitUntilElementIsVisible(SearchSalesAliasIcon1, _driver);
                    _driver.FindElement(SearchSalesAliasIcon1).Click();
                    logger.Info("TC_OC - Search Sales Alias icon is clicked");

                    FindElement(searchSalesAliasSearchTextBox, _driver);
                    searchSalesAliasSearchTextBox.SendKeys(SalesAlias);
                    logger.Info("TC_OC - Enter Sales Alias Information for Search" + "(" + SalesAlias + ")");

                    FindElement(searchSalesAliasSearchButton, _driver);
                    searchSalesAliasSearchButton.Click();
                    logger.Info("TC_OC - Search Button Clicked");

                    FindElement(sASearchResult, _driver);
                    sASearchResult.Click();
                    WaitForPage();
                    logger.Info("TC_OC - Sales Alias is selected from search result");

                }
                else
                {
                    Console.WriteLine("element not present -- so it entered the else loop");
                    logger.Info("No such element - so it entered into the else loop");
                    WaitForPage();
                }


            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }
        public void AddSalesAlias4(string SalesAlias)
        {
            try
            {
                IList<IWebElement> SearchSalesAlias = _driver.FindElements(By.CssSelector("#product_tr_4>td>div>span>a.search-sales-alias"));
                if (SearchSalesAlias.Count != 0)
                {
                    WaitForPage();
                    WaitUntilElementIsVisible(SearchSalesAliasIcon1, _driver);
                    _driver.FindElement(SearchSalesAliasIcon1).Click();
                    logger.Info("TC_OC - Search Sales Alias icon is clicked");

                    FindElement(searchSalesAliasSearchTextBox, _driver);
                    searchSalesAliasSearchTextBox.SendKeys(SalesAlias);
                    logger.Info("TC_OC - Enter Sales Alias Information for Search" + "(" + SalesAlias + ")");

                    FindElement(searchSalesAliasSearchButton, _driver);
                    searchSalesAliasSearchButton.Click();
                    logger.Info("TC_OC - Search Button Clicked");

                    FindElement(sASearchResult, _driver);
                    sASearchResult.Click();
                    logger.Info("TC_OC - Sales Alias is selected from search result");

                }
                else
                {
                    Console.WriteLine("element not present -- so it entered the else loop");
                    logger.Info("No such element - so it entered into the else loop");
                    WaitForPage();
                }


            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }
        public void AddPurchaseAlias(string PurchaseAlias)
        {
            try
            {
                Actions cursor1 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {
                        FindElement(purchaseAlias1, _driver);
                        purchaseAlias1.Click();
                        WaitForPage();

                        IWebElement Fliter_Purchase_Alias_TextBox = _driver.FindElement(By.CssSelector(".purchase_alias_data.spmh310.open>div>div>input"));

                        FindElement(Fliter_Purchase_Alias_TextBox, _driver);
                        Fliter_Purchase_Alias_TextBox.SendKeys(PurchaseAlias);

                        Fliter_Purchase_Alias_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Purchase Alias is selected as" + "(" + PurchaseAlias + ")");


                        cursor1.MoveToElement(Fliter_Purchase_Alias_TextBox).Build().Perform();
                        WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }
                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }
        }

        public void AddPurchaseAlias4(string PurchaseAlias)
        {
            try
            {
                Actions cursor1 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {
                        FindElement(purchaseAlias4, _driver);
                        purchaseAlias4.Click();
                        WaitForPage();

                        IWebElement Fliter_Purchase_Alias_TextBox = _driver.FindElement(By.CssSelector(".purchase_alias_data.spmh310.open>div>div>input"));

                        FindElement(Fliter_Purchase_Alias_TextBox, _driver);
                        Fliter_Purchase_Alias_TextBox.SendKeys(PurchaseAlias);

                        Fliter_Purchase_Alias_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Purchase Alias is selected as" + "(" + PurchaseAlias + ")");
                        //WaitForPage();

                        cursor1.MoveToElement(Fliter_Purchase_Alias_TextBox).Build().Perform();
                        //WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }
                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }

        }
        public void AddSupplier(string Supplier)
        {
            try
            {

                Actions cursor2 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {

                        IWebElement Supplier_1 = _driver.FindElement(By.XPath("//*[@id='oc_sup_hold_1']/div/button/span[1]"));
                        FindElement(Supplier_1, _driver);
                        Supplier_1.Click();
                        WaitForPage();

                        IWebElement Fliter_Supplier_TextBox = _driver.FindElement(By.CssSelector(".supplier.spmh310.open>div>div>input"));
                        FindElement(Fliter_Supplier_TextBox, _driver);
                        Fliter_Supplier_TextBox.SendKeys(Supplier);

                        Fliter_Supplier_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Supplier is selected as" + "(" + Supplier + ")");
                        WaitForPage();

                        cursor2.MoveToElement(Fliter_Supplier_TextBox).Build().Perform();
                        WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }

                }
                //CommonOperations.fluWait_clickable(driver, newOrder.Fliter_Supplier_TextBox);
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);
            }

        }
        public void AddSupplier4(string Supplier)
        {
            try
            {

                Actions cursor2 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {
                        IWebElement Supplier_4 = _driver.FindElement(By.XPath("//*[@id='oc_sup_hold_4']/div/button/span[1]"));
                        FindElement(Supplier_4, _driver);
                        Supplier_4.Click();
                        WaitForPage();

                        IWebElement Fliter_Supplier_TextBox = _driver.FindElement(By.CssSelector(".supplier.spmh310.open>div>div>input"));
                        FindElement(Fliter_Supplier_TextBox, _driver);
                        Fliter_Supplier_TextBox.SendKeys(Supplier);

                        Fliter_Supplier_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Supplier is selected as" + "(" + Supplier + ")");
                        WaitForPage();

                        cursor2.MoveToElement(Fliter_Supplier_TextBox).Build().Perform();
                        WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }

                }
                //CommonOperations.fluWait_clickable(driver, newOrder.Fliter_Supplier_TextBox);
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);
            }

        }
        public void AddTerminal(string Terminal)
        {
            try
            {
                Actions cursor3 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {
                        FindElement(terminal1, _driver);
                        terminal1.Click();
                        WaitForPage();

                        IWebElement Fliter_Terminal_TextBox = _driver.FindElement(By.CssSelector(".supply_point.terminalAutopopulate.spmh310.open>div>div>input"));
                        FindElement(Fliter_Terminal_TextBox, _driver);
                        Fliter_Terminal_TextBox.SendKeys(Terminal);

                        Fliter_Terminal_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Terminal is selected as" + "(" + Terminal + ")");
                        WaitForPage();

                        cursor3.MoveToElement(Fliter_Terminal_TextBox).Build().Perform();
                        WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }

                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }

        public void AddTerminal4(string Terminal)
        {
            try
            {
                Actions cursor3 = new Actions(_driver);
                for (int i = 0; i < 1; i++)
                {
                    try
                    {
                        FindElement(terminal4, _driver);
                        terminal4.Click();
                        WaitForPage();

                        IWebElement Fliter_Terminal_TextBox = _driver.FindElement(By.CssSelector(".supply_point.terminalAutopopulate.spmh310.open>div>div>input"));
                        FindElement(Fliter_Terminal_TextBox, _driver);
                        Fliter_Terminal_TextBox.SendKeys(Terminal);

                        Fliter_Terminal_TextBox.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Terminal is selected as" + "(" + Terminal + ")");
                        WaitForPage();

                        cursor3.MoveToElement(Fliter_Terminal_TextBox).Build().Perform();
                        WaitForPage();

                    }
                    catch (StaleElementReferenceException ex)
                    {
                        ex.ToString();
                        Console.WriteLine("Trying to recover from a stale element :" + ex);
                        logger.Error(ex);
                    }

                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void Quantity(string Qty)
        {
            try
            {

                IList<IWebElement> Qua = _driver.FindElements(By.XPath("//*[contains(@id,'quantity_1')]"));

                foreach (IWebElement t in Qua)
                {

                    if (t.Displayed)
                    {
                        t.Clear();
                        t.SendKeys(Qty);
                        t.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Quantity is entered as" + "(" + Qty + ")");
                        WaitForPage();
                    }
                }

                /*  if (!_driver.FindElement(By.CssSelector("#pump_required_1")).Selected)
                  {
                      IWebElement Pump = _driver.FindElement(By.CssSelector("#pump_required_1"));
                      //FindElement(Pump, _driver);
                      Pump.Click();
                      logger.Info("TC_OC - Pump Required enabled");
                      WaitForPage();
                  }*/

            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void Quantity4(string Qty)
        {
            try
            {

                IList<IWebElement> Qua = _driver.FindElements(By.XPath("//*[contains(@id,'quantity_4')]"));
                foreach (IWebElement t in Qua)
                {
                    if (t.Displayed)
                    {
                        t.Clear();
                        t.SendKeys(Qty);
                        t.SendKeys(Keys.Enter);
                        logger.Info("TC_OC - Quantity is entered as" + "(" + Qty + ")");
                        WaitForPage();
                    }
                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void ApplyPopupOkay()
        {
            try
            {
                WaitUntilElementIsVisible(NewOrderApplyButton, _driver);
                _driver.FindElement(NewOrderApplyButton).Click();
                logger.Info("TC_OC - Apply button is clicked");

                WaitUntilElementIsVisible(ApplyPOPupOKButton, _driver);
                _driver.FindElement(ApplyPOPupOKButton).Click();
                WaitForPageLoad();
                logger.Info("TC_OC - Alert window: Okay button is clicked");

                // The field value is retrieved by the getAttribute("id") Selenium WebDriver predefined method and assigned to the String object.

                Order_To_Search = _driver.FindElement(By.XPath("//*[@id='orderNoCell_1']")).Text;
                WaitForPage();
                Console.WriteLine(Order_To_Search);
                logger.Info("TC_OC - Newly created order number is generated as" + "(" + Order_To_Search + ")");

            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }

        public void SavePopupOkay()
        {
            try
            {
                WaitUntilElementIsVisible(OrderSaveButton, _driver);
                _driver.FindElement(OrderSaveButton).Click();
                logger.Info("TC_OC - Save Order button is clicked");

                WaitUntilElementIsVisible(ApplyPOPupOKButton, _driver);
                _driver.FindElement(ApplyPOPupOKButton).Click();
                //WaitForPageLoad();
                logger.Info("TC_OC - Alert window: Okay button is clicked");
                logger.Info("TC_OC - Order created successfully: Order Number#" + "(" + Order_To_Search + ")");

                _driver.Navigate().Refresh();
                logger.Info("Application is refreshed,Please wait till it loads...");

                isAlertpresent(_driver);
                //WaitForPage();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void OrderToSearch()
        {
            try
            {
                // The field value is retrieved by the getAttribute("id") Selenium WebDriver predefined method and assigned to the String object.
                Order_To_Search = _driver.FindElement(By.XPath("//*[@id='orderNoCell_1']")).Text;
                Console.WriteLine(Order_To_Search);
                logger.Info("TC_OC - Newly created order number is generated as" + "(" + Order_To_Search + ")");
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }
        }



        public void CloseOrderCreationWindow()
        {
            try
            {
                WaitUntilElementIsVisible(closeOrderCreationWindow, _driver);
                _driver.FindElement(closeOrderCreationWindow).Click();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void PumpRequired()
        {
            try
            {
                if (!_driver.FindElement(By.CssSelector("#pump_required_1")).Selected)
                {
                    _driver.FindElement(By.CssSelector("#pump_required_1")).Click();
                    logger.Info("TC_OC - Pump Required enabled");
                    WaitForPage();
                }
                else if (_driver.FindElement(By.CssSelector("#pump_required_1")).Selected)
                {
                    _driver.FindElement(By.CssSelector("#pump_required_1")).Click();
                    logger.Info("TC_OC - Pump Required enabled");
                    WaitForPage();
                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void GroupOrderCheckBox()
        {
            try
            {
                if (!groupOrderChechBox.Selected)
                {
                    groupOrderChechBox.Click();
                    logger.Info("TC_OC - Pump Required enabled");
                    WaitForPage();
                }
                else if (groupOrderChechBox.Selected)
                {
                    groupOrderChechBox.Click();
                    logger.Info("TC_OC - Pump Required enabled");
                    WaitForPage();
                }
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);

            }

        }
        public void UnscheduledOrderList()
        {
            try
            {
                _driver.FindElement(DispatchBoardButton).Click();
                logger.Info("TC_EO -Entering into Dispatchboard Module");
                WaitForPage();

                isAlertpresent(_driver);
                WaitForPage();

            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }
        }


        public void UnscheduledOrderListSort()
        {
            try
            {
                ordersnoSortUnscheduledOrderList.Click();
                logger.Info("TC_EO -Unscheduled Order List: Order No. column is sorted");


                ordersnoFilterUnscheduledOrderList.Click();
                logger.Info("TC_EO -Unscheduled Order List: Order No. column is selected");


                ordernoFilterTextboxUnscheduledOrderList.Click();
                logger.Info("TC_EO -Unscheduled Order List: Filtered by Order No#");
                WaitForPage();

                ordernoFilterTextboxUnscheduledOrderList.Clear();
                WaitForPage();

                ordernoFilterTextboxUnscheduledOrderList.SendKeys(Order_To_Search);
                ordernoFilterTextboxUnscheduledOrderList.SendKeys(Keys.Enter);
                logger.Info("TC_EO - Order No. is selected from unscheduled order list for edit:" + "(" + Order_To_Search + ")");
                WaitForPage();

                IWebElement OrderSelection = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr"));
                OrderSelection.Click();
                WaitForPage();

                Actions cursor = new Actions(_driver);
                cursor.ContextClick(OrderSelection).Build().Perform();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }
        }

        public void UnscheduledOrderEditOrderContextMenu()
        {
            try
            {
                unscheduledListContextMenuEditOrder.Click();
                logger.Info("TC_EO - Unscheduled List: Edit Order Context is Selected");
                WaitForPage();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }

        }

        public void UnscheduledOrderSelectRandomOrderNo()
        {
            try
            {
                //int RowCount = _driver.FindElements(By.XPath("//table[@id ='ds-unscheduled-orders']/tbody/tr/td[3]/div")).Count();

                int RowCount = _driver.FindElements(By.XPath("//table[@id='ds-unscheduled-orders']/tbody//tr[@class='ng-scope ng-hide']")).Count();
            line1: Random random = new Random();
                for (int i12 = 1; i12 < RowCount; i12++)
                {
                    goto line1;

                    string ss = _driver.FindElement(By.XPath("//table[@id ='ds-unscheduled-orders']/tbody/tr[" + i12 + "]")).Text;

                    //string ss = _driver.FindElement(By.XPath("//table[@id ='ds-unscheduled-orders']/tbody/tr/a[@group-id =]")).Text;

                    Random random1 = new Random();


                }



            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);


            }

        }

        public void UnscheduledOrderEditTripTimeContextMenu(string TripTime)
        {
            try
            {
                unscheduledListContextMenuEditTripTime.Click();
                logger.Info("Unscheduled List: Edit Trip Time Context is Selected");
                WaitForPage();

                /*WebElement elem = driver.findElement(By.xpath("//*[@id='db-uso-context-menu']/ul/li[4]/a")); 
                logger.info("Edit Trip Time Context Menu is Selected");
                 String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
                 ((JavascriptExecutor) driver).executeScript(js, elem);
                 elem.click();
                 Thread.sleep(3000);*/


                IWebElement Time = _driver.FindElement(By.XPath("//*[@id='global_trip_time']"));
                logger.Info("Edit Trip Time Screen");
                /*String js1 = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
                ((JavascriptExecutor) driver).executeScript(js1, Time);*/
                Time.Click();
                WaitForPage();

                _driver.FindElement(By.Id("global_trip_time")).SendKeys(Keys.Delete);
                _driver.FindElement(By.Id("global_trip_time")).SendKeys(Keys.Delete);
                _driver.FindElement(By.Id("global_trip_time")).SendKeys(Keys.Delete);
                _driver.FindElement(By.Id("global_trip_time")).SendKeys(Keys.Delete);
                WaitForPage();

                IWebElement set1 = _driver.FindElement(By.Id("global_trip_time"));
                logger.Info("Trip Time is Updated Successfully");
                set1.SendKeys(TripTime);
                WaitForPage();

                _driver.FindElement(By.Id("btn-save")).Click();
                logger.Info("Save Button is Clicked");
                WaitForPageLoad();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                logger.Error(e);
            }

        }

    }
}
