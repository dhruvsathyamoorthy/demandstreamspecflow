﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PDI.Demandstream.Logistics.Pages.ManageShift
{
	public class ManageShiftPage : BasePage
	{
		public RemoteWebDriver _driver = LoginPage._driver;

		private static readonly log4net.ILog logger =
		log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		By Select_Driver => By.XPath("//button[@data-id='driver_list']");
		By Shift_Calender_Month_Year => By.Id("date-label");
		By ManageShift_DispatchBoard_Button => By.Id("manage_shift");
		By Select_Vehicle => By.XPath("//button[@data-id='truck_list']");
		By Previous_Month => By.XPath("//a[@data-original-title='Previous Month']");
		By Current_Month => By.XPath("//a[@data-original-title='Current Month']");
		By Next_Month => By.XPath("//a[@data-original-title='Next Month']");
		By Create_Shift => By.Id("eventCreate");
		By Shift_Maintenance_Cancel => By.Id("btn-cancel");
		By New_Shift_Event_Fields => By.Id("shift_process_fields");
		By Shift_Name => By.Id("shift_name");
		By Shift_Start_Date => By.Id("shift_start_date");
		By Shift_Start_Time => By.XPath("//button[@data-id='shift_start_time']");
		By Shift_End_Time => By.XPath("//button[@data-id='shift_end_time']");
		By Shift_Event => By.XPath("//button[@data-id='shift_type']");
		By Repeats_CheckBox => By.Id("shift_repeat_on");
		By Rolling_CheckBox => By.Id("shift_rooling_on");
		By Repeat_On_Sunday => By.Id("week_day_1");
		By Repeat_On_Monday => By.Id("week_day_2");
		By Repeat_On_Tuesday => By.Id("week_day_3");
		By Repeat_On_Wednesday => By.Id("week_day_4");
		By Repeat_On_Thuresday => By.Id("week_day_5");
		By Repeat_On_Friday => By.Id("week_day_6");
		By Repeat_On_Saturday => By.Id("week_day_7");
		By Rolling_Days_ON => By.XPath("//input[@label-name-on='rolling_days_on']");
		By Rolling_Days_Off => By.XPath("//input[@label-name-off='rolling_days_off']");
		By Add_Rolling_Table_Row => By.Id("add_rolling");
		By Remove_Rolling_Table_Row => By.CssSelector(".remove_rolling_shift");
		By Shift_End_Date => By.Id("shift_end_date");
		By Select_ShiftEvent_Driver_Name => By.XPath("//button[@data-id='shift_m_driver']");
		By Select_ShiftEvent_Vehicle_Name => By.XPath("//button[@data-id='shift_m_truck']");
		By Shift_Trailer_Name => By.Id("shift_trailer");
		By New_Shift_Event_Cancel => By.Id("btn-cancel");
		By New_Shift_Event_Save => By.Id("btn-save");
		By New_Shift_Event_Back => By.Id("btn-back");
		By Edit_Shift_Event_Confirm => By.Id("btn-confirm");
		By Edit_Shift_Event_Delete => By.Id("btn-delete");
		By New_Shift_Event_Confirmation_Popup => By.Id("shift_confirm_container");
		By New_Shift_Event_Conflict_Popup => By.Id("conflicting_shift_container");
		By Confirm_Popup_Cancel_Button => By.XPath("//div[@class='jconfirm-box']/div[@class='buttons']/button[1]");
		By Confirm_Popup_Ok_Button => By.XPath("//div[@class='jconfirm-box']/div[@class='buttons']/button[2]");
		By Shift_DropDown_TextBox => By.CssSelector("div.dropdown-menu:nth-child(1)>div:nth-child(1)>input:nth-child(1)"); //.bs-searchbox>input
		By Shift_DropDown_Value => By.CssSelector(".dropdown-menu.open>ul>li>a>span");
		By Manage_shift_button => By.Id("manage_shift");
		public ManageShiftPage()
		{

		}

		public void CreateSingleShift()
		{
			var DriverList = new List<string>();
			DriverList = ExistingDriver(_driver);


			List<string> TruckList_final = new List<string>();
			TruckList_final = TrucksUnderUse(_driver);


			WaitUntilElementIsVisible(Manage_shift_button, _driver);
			_driver.FindElement(Manage_shift_button).Click();


			WaitUntilElementIsVisible(Create_Shift, _driver);
			_driver.FindElement(Create_Shift).Click();

			Thread.Sleep(1000);
			//CommonOperations.fluWait_visible(driver, _driver.FindElement(New_Shift_Event_Fields);
			//CommonOperations.fluWait_clickable(driver, _driver.FindElement(New_Shift_Event_Save);

			WaitUntilElementIsVisible(Shift_Name, _driver);
			_driver.FindElement(Shift_Name).SendKeys("Automated Single Shift");

			WaitUntilElementIsVisible(Shift_End_Time, _driver);
			_driver.FindElement(Shift_End_Time).Click();

			WaitUntilElementIsVisible(Shift_DropDown_TextBox, _driver);
			_driver.FindElement(Shift_DropDown_TextBox).SendKeys("11:30 PM");
			_driver.FindElement(Shift_DropDown_TextBox).SendKeys(Keys.Enter);

			WaitUntilElementIsVisible(Shift_Name, _driver);
			_driver.FindElement(Shift_Name).Click();

			WaitUntilElementIsVisible(Select_ShiftEvent_Driver_Name, _driver);
			_driver.FindElement(Select_ShiftEvent_Driver_Name).Click();

			WaitUntilElementIsVisible(Shift_DropDown_TextBox, _driver);
			_driver.FindElement(Shift_DropDown_TextBox);

			string Drivername1 = null;

			var FinAllDriverList = new List<string>();
			FinAllDriverList = FreeDrivers(_driver, DriverList);

			foreach (string ADL in FinAllDriverList)
			{
				if (!ADL.Contains("Do") && !ADL.Equals("Select Driver"))
				{
					Drivername1 = ADL;
					Thread.Sleep(2000);
					_driver.FindElement(Shift_DropDown_TextBox).SendKeys(ADL);
					_driver.FindElement(Shift_DropDown_TextBox).SendKeys(Keys.Enter);

					break;
				}

			}
			Thread.Sleep(2000);
			_driver.FindElement(Select_ShiftEvent_Driver_Name).Click();
			_driver.FindElement(Select_ShiftEvent_Vehicle_Name).Click();

			WaitUntilElementIsVisible(Shift_DropDown_TextBox, _driver);
			_driver.FindElement(Shift_DropDown_TextBox);
			
			IList<IWebElement> AllTrucks = _driver.FindElements(By.CssSelector(".btn-group.bootstrap-select.spmh310.dropup.open>div>ul>li"));
			var FreeTrucksName = new List<string>();

			FreeTrucksName = FreeTrucks(_driver, AllTrucks);


			foreach (string t in FreeTrucksName)
			{
				Console.WriteLine(t);
			}

				for (int i = 0; i < 2;)
				{
					if (FreeTrucksName.Count.Equals("Select Vehicle"))
					{
					i++;
					}
				else
				{
					Console.WriteLine(FreeTrucksName[i]);
					Thread.Sleep(3000);
					_driver.FindElement(Shift_DropDown_TextBox).SendKeys(FreeTrucksName[i]);
					_driver.FindElement(Shift_DropDown_TextBox).SendKeys(Keys.Enter);
					break ;
				}

			}

			_driver.FindElement(New_Shift_Event_Save).Click();
			Thread.Sleep(3500);
			_driver.FindElement(New_Shift_Event_Save).Click();
			Thread.Sleep(3500);

			Actions cursor = new Actions(_driver);
			cursor.MoveToElement(_driver.FindElement(By.XPath("//div[@class='jconfirm-box']/div[@class='buttons']/button[2]"))).Click().Build().Perform();
			Thread.Sleep(2500);

			_driver.FindElement(Select_Driver).Click();
			_driver.FindElement(Shift_DropDown_TextBox).SendKeys(Drivername1);
			_driver.FindElement(Shift_DropDown_TextBox).SendKeys(Keys.Enter);
			Thread.Sleep(4500);
			_driver.FindElement(By.Id("btn-cancel")).Click();
		}

		public static List<string> ExistingDriver(RemoteWebDriver _driver)
		{
			IList<IWebElement> ExistingDrivers = _driver.FindElements(By.CssSelector(".ds-driver-name>span"));
			var DriverList = new List<string>();
			try
			{
				foreach (IWebElement t in ExistingDrivers)
				{
					string[] myArray = t.Text.Split(" ".ToCharArray());
					string newstring = myArray[1] + " " + myArray[0];
					newstring = newstring.Trim().Replace("(", "").Replace(")", "").Replace("\"", "").Replace("'", "").Replace(",", "");
					DriverList.Add(newstring);

				}
			}
			catch (Exception e)
			{
				//TODO Auto-generated catch block
				e.ToString();
			}

			// Remove Duplicates from Drivers in Dispatch Board.
			for (int i = 0; i < DriverList.Count; i++)
			{
				for (int j = i + 1; j < DriverList.Count; j++)
				{
					if (DriverList[i].Equals(DriverList[j]))
					{
						DriverList.Remove(j.ToString());
					}
				}
			}
			return DriverList;

		}

		public static List<string> TrucksUnderUse(RemoteWebDriver _driver)
		{

			//Fetch Trucks under Use

			IList<IWebElement> UsedTrucks = _driver.FindElements(By.CssSelector("div.truck-name"));
			var TruckList = new List<string>();
			var TruckList_final = new List<string>();
			foreach (IWebElement t in UsedTrucks)
			{

				string[] myarray = t.Text.Split("\\(".ToCharArray());
				if (myarray.Length == 2)
				{
					myarray[1] = "";
					TruckList.Add(myarray[0].Trim() + myarray[1]);
				}

				else
				{
					TruckList.Add(myarray[0].Trim());
				}

			}

			foreach (string t in TruckList)
			{

				TruckList_final.Add(t.Trim());
				Console.WriteLine(t.Trim());
			}

			return TruckList_final;
		}

		public static List<string> FreeDrivers(RemoteWebDriver _driver, List<string> DriverList)
		{

			IList<IWebElement> AllDrivers = _driver.FindElements(By.XPath("/html/body/div[25]/div/ul/li/a"));
			//div [@class='dropdown-menu open']	[@style ='max-height: 373px; overflow: hidden;']/ul [@class ='dropdown-menu inner']/li/a/span [@class='text']"));
			Thread.Sleep(3000);

			var AllDriverList = new List<string>();
			foreach (IWebElement t in AllDrivers)
			{
				AllDriverList.Add(t.Text);
			}
			var FinAllDriverList = new List<string>();
			foreach (string ADL in AllDriverList)
			{
				FinAllDriverList.Add(ADL);
			}


			Thread.Sleep(3000);
			foreach (string t in DriverList)
			{
				foreach (string ADL in AllDriverList)
				{
					string[] targ = t.Split(" ".ToCharArray());
					Console.WriteLine(targ[0]);
					if (ADL.Contains(targ[0]))
					{
						FinAllDriverList.Remove(t);
					}
				}

			}

			return FinAllDriverList;
		}

		public static List<string> FreeTrucks(RemoteWebDriver _driver, IList<IWebElement> AllTrucks)
		{
			var TruckList_final = new List<string>();
			TruckList_final = TrucksUnderUse(_driver);

			var AllTruckName = new List<string>();
			foreach (IWebElement t in AllTrucks)
			{
				string[] myarray = t.Text.Split("\\(".ToCharArray());
				AllTruckName.Add(myarray[0]);

			}
			var FreeTrucksName = new List<string>();
			foreach (string t in AllTruckName)
			{
				FreeTrucksName.Add(t);
			}
			foreach (string AT in AllTruckName)
			{
				foreach (string TF in TruckList_final)
				{
					if (AT.Equals(TF))
					{
						FreeTrucksName.Remove(TF);
					}
				}
			}
			return FreeTrucksName;
		}


	}
}