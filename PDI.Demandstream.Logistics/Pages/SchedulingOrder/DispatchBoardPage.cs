﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using PDI.Demandstream.Logistics.Pages.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PDI.Demandstream.Logistics.Pages
{
    public class DispatchBoardPage : BasePage
    {
        public RemoteWebDriver _driver = LoginPage._driver;

        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        By DispatchBoardButton => By.LinkText("Dispatch Board");
        By UserMenuList => By.XPath("//a[@class='dropdown-toggle pad-lt-rt-0 header-un']//i[@class='fa fa-sort-desc']");
        By ReleaseHistory => By.XPath("//a[contains(text(),'Release History')]");
        By ClientSetting => By.XPath("//a[contains(text(),'Client Settings')]");
        By ClientSettingSearch => By.XPath("//input[@placeholder='Filter Client Settings']");
        By ClientSettingSaveButton => By.XPath("//div[@class='row dscs-bot-btn1']//button[@id='btn-apply']");
        By Logout => By.XPath("//a[@class='header-logout ds_resTab']");
        By DispatchBoardToogleButton => By.XPath("//tr[46]//td[2]//label[1]//span[1]");
        By DispatchToDriverToogleBT => By.XPath("//div[@class='pull-right ds-switch-hold']//span[@class='switch-label']");
        By ClientSettingSearchBt => By.XPath(" //span[2]//a[1]");


        //Unscheduled Order List
        By OrdersnoSortUnscheduledOrderList => By.Id("s_ono");
        By OrdersnoFilterUnscheduledOrderList => By.Id("f_ono");
        By OrdernoFilterTextboxUnscheduledOrderList => By.Id("ng-filter-orderno");
        By UnscheduledListContextMenuEditOrder => By.XPath("//*[@id='db-uso-context-menu']/ul/li[1]/a");
        By DispatchBoardDispatchboardButton => By.Id("s_ono");
        By UnscheduledListContextMenuEditTripTime => By.XPath("//div[@id='db-uso-context-menu']/ul/li[3]/a");
        By CloseOrderCreationWindow => By.XPath("//button[@class='close']");
        By GroupOrderChechBox => By.XPath("//input[@id='isGroupOrder']");

        public DispatchBoardPage()
        {

        }

        public void DispatchToDriverBoard()
        {

            try
            {

                WaitUntilElementIsVisible(UserMenuList, _driver);
                _driver.FindElement(UserMenuList).Click();

                WaitUntilElementIsVisible(ReleaseHistory, _driver);
                _driver.FindElement(ReleaseHistory).Click();
                logger.Info("");

                WaitForPage();

                WaitUntilElementIsVisible(ClientSetting, _driver);
                _driver.FindElement(ClientSetting).Click();

                WaitUntilElementIsVisible(ClientSettingSearch, _driver);
                _driver.FindElement(ClientSettingSearch).SendKeys("Dispatch To Driver");
                WaitUntilElementIsVisible(ClientSettingSearch, _driver);
                _driver.FindElement(ClientSettingSearch).SendKeys(Keys.Enter);

                // WaitUntilElementIsVisible(ClientSettingSearchBt, _driver);
                // _driver.FindElement(ClientSettingSearchBt).Click();

                WaitForPage();

                Boolean DispatchToDriverBoardToogleButton = _driver.FindElement(By.Id("dispatch_to_driver")).Selected;

                if (DispatchToDriverBoardToogleButton == false)

                {
                    WaitUntilElementIsVisible(DispatchBoardToogleButton, _driver);
                    _driver.FindElement(DispatchBoardToogleButton).Click();

                    WaitUntilElementIsVisible(ClientSettingSaveButton, _driver);
                    _driver.FindElement(ClientSettingSaveButton).Click();

                    WaitUntilElementIsVisible(UserMenuList, _driver);
                    _driver.FindElement(UserMenuList).Click();

                    WaitUntilElementIsVisible(Logout, _driver);
                    _driver.FindElement(Logout).Click();

                    LoginPage loginPage = new LoginPage();
                    loginPage.LaunchDemandStreamApp();

                }
                else
                {
                    _driver.FindElementByXPath("//img[@class='normal-logo']").Click();
                    Console.WriteLine("Toogle is already switched ON");
                }

            }
            catch (Exception e)
            {
                Assert.AreEqual(false, true, e.Message);
                _driver.Navigate().Refresh();
                logger.Error(e);


            }
        }

        public void RanadomOrderandEditTripTime(string TripTime)
        {

            try
            {
                WaitUntilElementIsVisible(DispatchBoardButton, _driver);
                _driver.FindElement(DispatchBoardButton).Click();
                logger.Info("Clicking the Dispatch Button ");
                isAlertpresent(_driver);

                isAlertpresent(_driver);
                WaitForPage();

                WaitUntilElementIsVisible(OrdersnoSortUnscheduledOrderList, _driver);
                _driver.FindElement(OrdersnoSortUnscheduledOrderList).Click();
                logger.Info("TC_EO -Unscheduled Order List: Order No. column is sorted");

                WaitUntilElementIsVisible(OrdersnoFilterUnscheduledOrderList, _driver);
                _driver.FindElement(OrdersnoFilterUnscheduledOrderList).Click();
                logger.Info("TC_EO -Unscheduled Order List: Order No. column is selected");

                WaitUntilElementIsVisible(OrdernoFilterTextboxUnscheduledOrderList, _driver);
                _driver.FindElement(OrdernoFilterTextboxUnscheduledOrderList).Click();
                logger.Info("TC_EO -Unscheduled Order List: Filtered by Order No#");
                WaitForPage();

                IList<IWebElement> Order = _driver.FindElements(By.XPath("//ul[@class ='dropdown-menu ds-checkbox-list form-group filter_ono']/li"));
                var Order_In_UnscheduleList = new List<string>();

                foreach (IWebElement temp in Order)
                    if (temp.Text.Trim() != "No Open orders found!")
                    {
                        Order_In_UnscheduleList.Add(temp.Text);
                    }
                    else
                    {
                        Console.WriteLine("Order No# is not present");
                        logger.Info("Order No# is not present to schedule the order to truck");
                    }
                for (int i = 0; i < Order.Count; i++)
                {
                    IWebElement Temp1 = Order[i];
                    Order_In_UnscheduleList.Add(Temp1.Text);
                }
                WaitForPage();

                WaitUntilElementIsVisible(OrdernoFilterTextboxUnscheduledOrderList, _driver);
                _driver.FindElement(OrdernoFilterTextboxUnscheduledOrderList).Clear();
                WaitForPage();

                Random random = new Random();
                int Order_To_Search = random.Next(Order_In_UnscheduleList.Count);

                _driver.FindElement(OrdernoFilterTextboxUnscheduledOrderList).SendKeys(Order_In_UnscheduleList[Order_To_Search]);
                _driver.FindElement(OrdernoFilterTextboxUnscheduledOrderList).SendKeys(Keys.Enter);
                Console.WriteLine("TC_EO - Order No. is selected from unscheduled order list for edit:" + "(" + Order_In_UnscheduleList[Order_To_Search] + ")");
                WaitForPage();

                //IWebElement OrderSelection = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr:nth-child(1)"));
                IWebElement OrderSelection = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr"));
                //FindElement(OrderSelection, _driver);
                OrderSelection.Click();
                WaitForPage();

                Actions cursor = new Actions(_driver);
                cursor.ContextClick(OrderSelection).Build().Perform();

                WaitUntilElementIsVisible(UnscheduledListContextMenuEditTripTime, _driver);
                _driver.FindElement(UnscheduledListContextMenuEditTripTime).Click();
                logger.Info("Unscheduled List: Edit Trip Time Context is Selected");
                WaitForPage();

                IList<IWebElement> elements = _driver.FindElements(By.XPath("//*[@id='global_trip_time']")).ToList();
                if (elements.Count > 0)
                {
                    IWebElement Time = _driver.FindElement(By.XPath("//*[@id='global_trip_time']"));
                    FindElement(Time, _driver);
                    logger.Info("Edit Trip Time Screen");
                    /*String js1 = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
                    ((JavascriptExecutor) driver).executeScript(js1, Time);*/
                    Time.Click();
                    //WaitForPage();

                    IWebElement T1 = _driver.FindElement(By.Id("global_trip_time"));
                    FindElement(T1, _driver);
                    T1.SendKeys(Keys.Delete);

                    IWebElement T2 = _driver.FindElement(By.Id("global_trip_time"));
                    FindElement(T2, _driver);
                    T2.SendKeys(Keys.Delete);

                    IWebElement T3 = _driver.FindElement(By.Id("global_trip_time"));
                    FindElement(T3, _driver);
                    T3.SendKeys(Keys.Delete);

                    IWebElement T4 = _driver.FindElement(By.Id("global_trip_time"));
                    FindElement(T4, _driver);
                    T4.SendKeys(Keys.Delete);
                    //WaitForPage();

                    IWebElement set1 = _driver.FindElement(By.Id("global_trip_time"));
                    FindElement(set1, _driver);
                    logger.Info("Trip Time is Updated Successfully");
                    set1.SendKeys(TripTime);
                    WaitForPage();

                    IWebElement SaveBt = _driver.FindElement(By.Id("btn-save"));
                    FindElement(SaveBt, _driver);
                    SaveBt.Click();
                    logger.Info("Save Button is Clicked");
                    WaitForPage();
                }
                else
                {
                    _driver.FindElement(By.XPath("//button[@class='close']")).Click();
                    Console.WriteLine("Edit Trip Time is not present");
                    logger.Info("Order No# is not present to schedule the order to truck");
                    WaitForPage();
                }

                WaitUntilElementIsVisible(DispatchBoardButton, _driver);
                _driver.FindElement(DispatchBoardButton).Click();
                logger.Info("Clicking the Dispatch Button ");
                isAlertpresent(_driver);


                WaitForPage();
                //ordersnoFilterUnscheduledOrderList.Click();
                //logger.Info("Clicking filter in the Order No Column in Unscheduled order IList ");
                //WaitForPage();

                //ordernoFilterTextboxUnscheduledOrderList.Clear();
                //WaitForPage();


                //ordernoFilterTextboxUnscheduledOrderList.SendKeys(Order_In_UnscheduleList[Order_To_Search]);
                //ordernoFilterTextboxUnscheduledOrderList.SendKeys(Keys.Enter);
                //WaitForPage();
            }
            catch (Exception e)
            {
                //Assert.AreEqual(false, true, e.Message);
                _driver.Navigate().Refresh();
                logger.Error(e);


            }
        }
        
        public void ScheduleOrderToTruck()
        {

            Boolean DispatchToDriverBoardToogleBT = _driver.FindElement(By.Id("dd-switch")).Selected;

            if (DispatchToDriverBoardToogleBT == true)

            {
                WaitUntilElementIsVisible(DispatchToDriverToogleBT, _driver);
                _driver.FindElement(DispatchToDriverToogleBT).Click();
            }
            else
            {
                Console.WriteLine("Toogle is already switched ON");
            }

            isAlertpresent(_driver);
            WaitForPage();

            IList<IWebElement> SearchOrderNumber = _driver.FindElements(By.CssSelector("#ds-unscheduled-orders>tbody>tr")).ToList();
            foreach (var label in SearchOrderNumber)
                if (label.Text.Trim() != "No Open orders found!")
                {
                    logger.Info("Entering the Order No in the filter textbox");
                    IList<IWebElement> Trucks = _driver.FindElements(By.CssSelector(".shift-begining"));
                    IWebElement FilteredOrder1 = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr"));
                    FindElement(FilteredOrder1, _driver);

                    dragAndDropJS(FilteredOrder1, Trucks[0], _driver);
                    WaitForPage();
                    _driver.FindElement(By.XPath("//th[@class='un-order-item-header expender']"));
                    logger.Info("Scheduling the order ");
                    WaitForPage();
                }
                else
                {
                    Console.WriteLine("Order No# is not present");
                    logger.Info("Order No# is not present to schedule the order to truck");
                    WaitForPage();
                }
        }

        public void ScheduleOrderToDispatchToDriverBoard()
        {
            try
            {
                Boolean DispatchToDriverBoardToogleBT = _driver.FindElement(By.Id("dd-switch")).Selected;

                if (DispatchToDriverBoardToogleBT == false)

                {
                    WaitUntilElementIsVisible(DispatchToDriverToogleBT, _driver);
                    _driver.FindElement(DispatchToDriverToogleBT).Click();
                }
                else
                {
                    Console.WriteLine("Toogle is already switched ON");
                }

                isAlertpresent(_driver);
                WaitForPage();

                IList<IWebElement> SearchOrderNumber = _driver.FindElements(By.CssSelector("#ds-unscheduled-orders>tbody>tr")).ToList();
                foreach (var label in SearchOrderNumber)
                    if (label.Text.Trim() != "No Open orders found!")
                    {
                        logger.Info("Entering the Order No in the filter textbox");
                        IList<IWebElement> Trucks = _driver.FindElements(By.CssSelector(".dr-shift-begining"));
                        IWebElement FilteredOrder = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr"));
                        FindElement(FilteredOrder, _driver);

                        dragAndDropJS(FilteredOrder, Trucks[0], _driver);
                        WaitForPage();
                        _driver.FindElement(By.XPath("//th[@class='un-order-item-header expender']"));
                        logger.Info("Scheduling the order ");
                        WaitForPage();
                    }
                    else
                    {
                        Console.WriteLine("Order No# is not present");
                        logger.Info("Order No# is not present to schedule the order to truck");
                        WaitForPage();
                    }
            }
            catch (Exception e)
            {
                Assert.AreEqual(true, true, e.Message);
                _driver.Navigate().Refresh();
                logger.Error(e);
            }
        }
        public void ScheduleOrderToCommonCarrier()
        {
            try
            {
                Boolean DispatchToDriverBoardToogleBT = _driver.FindElement(By.Id("dd-switch")).Selected;

                if (DispatchToDriverBoardToogleBT == false)

                {
                    WaitUntilElementIsVisible(DispatchToDriverToogleBT, _driver);
                    _driver.FindElement(DispatchToDriverToogleBT).Click();
                }
                else
                {
                    Console.WriteLine("Toogle is already switched ON");
                }

                isAlertpresent(_driver);
                WaitForPage();

                IList<IWebElement> SearchOrderNumber = _driver.FindElements(By.CssSelector("#ds-unscheduled-orders>tbody>tr")).ToList();
                foreach (var label in SearchOrderNumber)
                    if (label.Text.Trim() != "No Open orders found!")
                    {
                        logger.Info("Entering the Order No in the filter textbox");
                        IList<IWebElement> Trucks = _driver.FindElements(By.CssSelector(".cc-flex-container"));
                        IWebElement FilteredOrder = _driver.FindElement(By.CssSelector("#ds-unscheduled-orders>tbody>tr"));
                        FindElement(FilteredOrder, _driver);

                        dragAndDropJS(FilteredOrder, Trucks[0], _driver);
                        WaitForPage();
                        _driver.FindElement(By.XPath("//th[@class='un-order-item-header expender']"));
                        logger.Info("Scheduling the order ");
                        WaitForPage();
                    }
                    else
                    {
                        Console.WriteLine("Order No# is not present");
                        logger.Info("Order No# is not present to schedule the order to truck");
                        WaitForPage();
                    }
            }
            catch (Exception e)
            {
                Assert.AreEqual(true, true, e.Message);
                _driver.Navigate().Refresh();
                logger.Error(e);
            }
        }
    }
}
