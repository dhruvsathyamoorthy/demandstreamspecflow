﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Demandstream.Logistics.Pages.Common
{
    public class AdminPage : BasePage
    {
        public RemoteWebDriver _driver = LoginPage._driver;
        public AdminPage(RemoteWebDriver driver) => _driver = driver;
        
        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        By UserMenuList => By.XPath("//a[@class='dropdown-toggle pad-lt-rt-0 header-un']//i[@class='fa fa-sort-desc']");
        By Admin => By.XPath("//a[contains(text(),'Admin')]");

        public AdminPage()
        {

        }
        public void AdminMode ()
        {
            WaitUntilElementIsVisible(UserMenuList, _driver);
            _driver.FindElement(UserMenuList).Click();

            WaitUntilElementIsVisible(Admin, _driver);
            _driver.FindElement(Admin).Click();

            _driver.SwitchTo().Window(_driver.WindowHandles.Last());
            WaitForPage();
        }
    }
}
