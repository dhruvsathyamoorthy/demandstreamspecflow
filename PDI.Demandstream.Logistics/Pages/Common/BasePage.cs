﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace PDI.Demandstream.Logistics.Pages
{
    public class BasePage
    {
        public BasePage()
        {

        }

        /// <summary>
        /// Waits  for an element to be visible, then returns it
        /// </summary>
        /// <param name="locator">The element to FindElement</param>
        /// <returns>WebDriver IWebElement</returns>
        public IWebElement FindElement(IWebElement locator, IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.ElementToBeClickable(locator));
            return locator;
        }

        public Func<IWebDriver, bool> VisibilityOfElementLocated(IWebElement locator)
        {
            return driver =>
            {
                try
                {
                    Thread.Sleep(1500);
                }
                catch (ThreadInterruptedException e)
                {
                    Console.WriteLine(e.Message);
                }
                IWebElement element = FindElement(locator, driver);
                return element.Displayed;
            };
        }
        public IWebElement WaitUntilElementIsVisible(By locator, RemoteWebDriver driver)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(70));
                wait.Until(ExpectedConditions.ElementIsVisible(locator)); //Wait finishes when return is a non-null value or 'true'
                return driver.FindElement(locator);
            }
            catch (NoSuchElementException ex)
            {
                Console.WriteLine($"Element: {locator} was not found on current page.");
                Console.WriteLine(ex);
                return null;
            }
            catch (WebDriverTimeoutException)
            {
                Console.WriteLine($"Timed out Looking for Clickable Element; {locator}: Waited for {70} seconds.");
                return null;
            }
        }

        public IWebElement WaitUntilElementToBeClickable(By locator, RemoteWebDriver driver)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(70));
                wait.Until(ExpectedConditions.ElementToBeClickable(locator)); //Wait finishes when return is a non-null value or 'true'
                return driver.FindElement(locator);
            }
            catch (NoSuchElementException ex)
            {
                Console.WriteLine($"Element: {locator} was not found on current page.");
                Console.WriteLine(ex);
                return null;
            }
            catch (WebDriverTimeoutException)
            {
                Console.WriteLine($"Timed out Looking for Clickable Element; {locator}: Waited for {70} seconds.");
                return null;
            }
        }
        public void WaitForPageLoad()
        {
            Thread.Sleep(7000);
        }
        public void WaitForPage()
        {

            Thread.Sleep(1000);
        }
        public static void isAlertpresent(IWebDriver driver)
        {
            try
            {
                IAlert window = driver.SwitchTo().Alert();
                String content = window.Text;
                window.Accept();
                //logger.info("Alert Appeared & handeled : " + content);
                Console.WriteLine("Alert Appeared & handeled : " + content);
            }
            catch (Exception e)
            {
                //logger.info("No Alert Appeared");
                Console.WriteLine("No Alert Appeared");

            }
        }
        public static void dragAndDropJS(IWebElement source, IWebElement destination, IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("function createEvent(typeOfEvent) {\n" + "var event =document.createEvent(\"CustomEvent\");\n"
                + "event.initCustomEvent(typeOfEvent,true, true, null);\n" + "event.dataTransfer = {\n" + "data: {},\n"
                + "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n" + "getData: function (key) " +
                "{\n" + "return this.data[key];\n" + "}\n" + "};\n" + "return event;\n" + "}\n" + "\n" + "function " +
                "dispatchEvent(element, event,transferData) {\n" + "if (transferData !== undefined) " +
                "{\n" + "event.dataTransfer = transferData;\n" + "}\n" + "if (element.dispatchEvent) " +
                "{\n" + "element.dispatchEvent(event);\n" + "} else if (element.fireEvent) " +
                "{\n" + "element.fireEvent(\"on\" + event.type, event);\n" + "}\n" + "}\n" +
                "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n" + "var dragStartEvent =createEvent('dragstart');" +
                "\n" + "dispatchEvent(element, dragStartEvent);\n" + "var dropEvent = createEvent('drop');" +
                "\n" + "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);" +
                "\n" + "var dragEndEvent = createEvent('dragend');\n" + "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);" +
                "\n" + "}\n" + "\n" + "var source = arguments[0];\n" + "var destination = arguments[1];" +
                "\n" + "simulateHTML5DragAndDrop(source,destination);", source, destination);
            Thread.Sleep(1500);

        }
        
    }
}
