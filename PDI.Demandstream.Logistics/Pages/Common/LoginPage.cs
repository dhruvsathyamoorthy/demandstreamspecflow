﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace PDI.Demandstream.Logistics.Pages.Common
{
    public class LoginPage
    {

        public static IObjectContainer _objectContainer;

        public static RemoteWebDriver _driver ;

        public LoginPage(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        By Username => By.Id("login");
        By PassWord => By.Id("password");
        By Login => By.Id("login_form");
        By LoginedUser => By.XPath("//*[@id=>'mainNav']/div/div[3]/div/span/ul/li/a");
        By LoginedSuccess => By.XPath("//span[contains(text(),'Welcome')]");
        By LoginedFailed => By.CssSelector("# check_validation_error");
        By LogoutSuccess => By.CssSelector("//a[contains(text(),'FIRESTREAM WORLDWIDE')]");
        By ForgotPassword => By.LinkText("Forgot Username/Password?");
        By LearnMore => By.LinkText("Learn More");
        By ContactUS => By.LinkText("Contact Us");
        By AboutUs => By.LinkText("ABOUT US");
        By FirestreamWorldWide => By.LinkText("FIRESTREAM WORLDWIDE");

        public LoginPage()
        {
        }
       

        public void EnterUserNameAndPassword(string UserName, string Password)
        {
            _driver.FindElement(Username).SendKeys(UserName);
            _driver.FindElement(PassWord).SendKeys(Password);
        }


        public void Submit()
        {
            _driver.FindElement(Login).Click();
        }


       /* public  void SelectBrowser(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    ChromeOptions option = new ChromeOptions();
                    //option.AddArgument("--headless");
                    _driver = new ChromeDriver(option);
                    //_objectContainer.RegisterInstanceAs<IWebDriver>(_driver);
                    break;
                case BrowserType.Firefox:
                    var driverDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(driverDir, "geckodriver.exe");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    service.HideCommandPromptWindow = true;
                    service.SuppressInitialDiagnosticInformation = true;
                    _driver = new FirefoxDriver(service);
                    _objectContainer.RegisterInstanceAs<RemoteWebDriver>(_driver);
                    break;
                case BrowserType.IE:
                    break;
                default:
                    break;
            }
        }*/

        public  void LaunchDemandStreamApp()
        {
            ChromeOptions option = new ChromeOptions();
            _driver = new ChromeDriver(option);
           
            _driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["AppUrl"]);
            _driver.SwitchTo().Window(_driver.WindowHandles.Last());
            _driver.Manage().Window.Maximize();
            string userName = ConfigurationManager.AppSettings["UserName"];
            //logger.Info("UserName" + userName);

            string password = ConfigurationManager.AppSettings["Password"];
            //logger.Info("Password" + password);

            //LoginPage loginPage = new LoginPage();
            EnterUserNameAndPassword(userName, password);
            Submit();


        }
        public  void LandingScreen()
        {
            try
            {
                if (!_driver.Url.Contains("dispatchboard"))
                {
                    IWebElement dispatchBoardButton = _driver.FindElementByLinkText("Dispatch Board");
                    dispatchBoardButton.Click();
                    Console.WriteLine("TC_OC - Entering into Dispatch Board Module");
                    Thread.Sleep(1000);

                }
            }
            catch (Exception e)
            {

                //Assert.AreEqual(false, true, e.Message);
                //_driver.Navigate().Refresh();
                Console.WriteLine("Error :" + e);

            }
        }

    }
/*
     enum BrowserType
    {
        Chrome,
        Firefox,
        IE
    }*/
}

